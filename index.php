<?php
	function fromSQLDate($date) {
		return date('d/m/Y', strtotime($date));
	}

	function toSQLDate($date) {
		$ddate = str_replace('/', '-', $date);
		return date('Y-m-d', strtotime($ddate));
	}

	function getDay($date) {
		return intval(date('j', strtotime($date)));
	}

	if (isset($_GET['r'])) {
		$r = explode('/', $_GET['r']);
		if ($r[0] === 'api') {
			if (isset($r[1]) && $r[1] === 'people') {
				if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['name']) && isset($_POST['birthdate'])) {
					$db = @new mysqli('localhost', 'root', '', 'dummy');
					if ($db->connect_errno) {
						echo json_encode(array('errors'=>'can\'t connect database'));
						exit;
					}
					$result = $db->query('insert into people (name, birthdate) values ("' . $_POST['name'] . '", "' . toSQLDate($_POST['birthdate']) . '")');
					if (!$result) {
						echo json_encode(array('errors'=>'query failed'));
					}
					else {
						echo json_encode(array('id'=>$db->insert_id, 'name'=>$_POST['name'], 'birthdate'=>$_POST['birthdate']));
					}
					$db->close();
					exit;
				}
				elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && !isset($r[2])) {
					$db = @new mysqli('localhost', 'root', '', 'dummy');
					if ($db->connect_errno) {
						echo json_encode(array('errors'=>'can\'t connect database'));
						exit;
					}
					$result = $db->query('select * from people');
					if (!$result) {
						echo json_encode(array('errors'=>'query failed'));
					}
					else {
						$rows = array();
						while($row = $result->fetch_array()) {
							array_push($rows, array('id'=>$row['id'], 'name'=>$row['name'], 'birthdate'=>fromSQLDate($row['birthdate'])));
						}
						echo json_encode($rows);
					}
					$result->free();
					$db->close();
					exit;
				}
				elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($r[2])) {
					$db = @new mysqli('localhost', 'root', '', 'dummy');
					if ($db->connect_errno) {
						echo json_encode(array('errors'=>'can\'t connect database'));
						exit;
					}
					$result = $db->query('select * from people where id=' . $r[2]);
					$row = $result->fetch_array();
					if (!$row) {
						echo json_encode(array('errors'=>'query failed'));
					}
					else {
						echo json_encode(array('id'=>$row['id'], 'name'=>$row['name'], 'birthdate'=>fromSQLDate($row['birthdate'])));
					}
					$result->free();
					$db->close();
					exit;
				}
				elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE' && isset($r[2])) {
					$db = @new mysqli('localhost', 'root', '', 'dummy');
					if ($db->connect_errno) {
						echo json_encode(array('errors'=>'can\'t connect database'));
						exit;
					}
					$row = $db->query('select * from people where id=' . $r[2]);
					$item = $row->fetch_array();
					if (!$item) {
						echo json_encode(array('errors'=>'query failed'));
					}
					else {
						$result = $db->query('delete from people where id=' . $r[2]);
						if (!$result) {
							echo json_encode(array('errors'=>'query failed'));
						}
						else {
							echo json_encode(array('id'=>$item['id'], 'name'=>$item['name'], 'birthdate'=>fromSQLDate($item['birthdate'])));
						}
					}
					$row->free();
					$db->close();
					exit;
				}
			}
			elseif (isset($r[1]) && $r[1] === 'device') {
				if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($r[2])) {
					$db = @new mysqli('localhost', 'root', '', 'dummy');
					if ($db->connect_errno) {
						echo json_encode(array('errors'=>'can\'t connect database'));
						exit;
					}
					$result = $db->query('select * from people where id=' . $r[2]);
					$row = $result->fetch_array();
					if (!$row) {
						echo json_encode(array('errors'=>'query failed'));
					}
					else {
						$day = getDay($row['birthdate']);
						if ($day % 6 == 0) {
							echo json_encode(array('OS'=>'apple iOS'));
						}
						elseif ($day % 2 == 0) {
							echo json_encode(array('OS'=>'blackberry'));
						}
						elseif ($day % 3 == 0) {
							echo json_encode(array('OS'=>'android'));
						}
						else  {
							echo json_encode(array('OS'=>'feature phone'));
						}
					}
					$result->free();
					$db->close();
					exit;
				}
			}
		}
	}
	echo json_encode(array('errors'=>'invalid command'));
?>